// task link https://gist.github.com/xanf/b0e8aef77cf3b39d4c12dbe68b48b6e3

const socket = new WebSocket('ws://xo.t.javascript.ninja/games');

const userIDs = new Set();
let userId;
const existingGames = new Set();
const playerGame = {};

socket.onclose = () => {
  console.log('DISCONNECTED');
};

socket.onmessage = function(payload) {
  // printMessage(payload.data);
};

socket.addEventListener('message', event => {
  const data = JSON.parse(event.data);
  if (data.action === 'join') {
    // userIDs.add(data.id);
    // userId = data.id;
    playerGame.playerId = data.id;
  }
  if (data.action === 'add') {
    existingGames.add(data.id);
    renderExistingGames();
  }
  if (data.action === 'remove') {
    existingGames.delete(data.id);
    renderExistingGames();
  }

  if (data.action === 'startGame') {
    // const userId = [...userIDs][0];
    window.location = `game.html?gameId=${data.id}&userId=${playerGame.playerId}&side=${data.side}`;
  }
  console.log('Message from WebSocket', data);
});

function renderExistingGames() {
  const ul = document.querySelector('#game-list');
  while (ul.firstChild) {
    ul.firstChild.remove();
  }
  Array.from(existingGames).forEach((gameId, i) => {
    const li = document.createElement('li');
    const a = document.createElement('a');
    a.href = '#';
    a.dataset.gameId = gameId;
    a.textContent = `Игра номер ${i + 1}`;
    a.addEventListener('click', joinGame);
    li.appendChild(a);
    ul.appendChild(li);
  });
}

async function joinGame(e) {
  const { gameId } = e.target.dataset;
  // const userId = [...userIDs][0];
  playerGame.gameId = gameId;
  const body = JSON.stringify(playerGame);
  const res = await fetch('http://xo.t.javascript.ninja/gameReady', {
    method: 'POST',
    headers: new Headers({ 'Content-Type': 'application/json' }),
    body
  });
  // const data = await res.text();
  if (!res.ok) {
    errorMessage('Не удалось присоединиться к игре');
  }
}

const createGameBtn = document.querySelector('#create-game');

createGameBtn.addEventListener('click', e => {
  createGameHandler(e);
});

async function createGameHandler(e) {
  createGameBtn.disabled = true;
  try {
    const res = await fetch('http://xo.t.javascript.ninja/newGame', { method: 'POST' });
    const data = await res.json();
    const { yourId } = data;
    if (yourId) {
      playerGame.gameId = data.yourId;
      modalMessage('Ожидаем начала игры');
      const body = JSON.stringify(playerGame);
      console.log('stringified body', body);
      const res = await fetch('http://xo.t.javascript.ninja/gameReady', {
        method: 'POST',
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body
      });
      console.log('gameReady', res);
      if (res.status === 401) {
        throw new Error('Ошибка старта игры: другой игрок не ответил');
      } else if (!res.ok && res.status !== 401) {
        throw new Error('Неизвестная ошибка старта игры');
      }
    } else {
      throw new Error('Ошибка создания игры');
    }
  } catch (err) {
    console.dir(err);
    createGameBtn.disabled = false;
    errorMessage(err.message);
  }
}

function errorMessage(title) {
  const block = document.querySelector('.alert.error');
  block.textContent = title;
  block.style.display = 'block';
}

function modalMessage(title) {
  document.querySelector('#modal-wrap').style.display = 'block';
  const modalTitle = document.querySelector('.modal > .title');
  modalTitle.textContent = title;
}

// movePolling();

// class Socket {
//   constructor() {
//     this.socket = new WebSocket('ws://xo.t.javascript.ninja/games');
//     this.socket.onopen = Socket.onOpen;
//   }
//   static onOpen() {
//     document.querySelector('h1').innerHTML = 'Socket is open';
//   }
// }

// function connect() {
//   return new Promise((resolve, reject) => {
//     const socket = new WebSocket('ws://xo.t.javascript.ninja/games');
//     socket.onopen = () => resolve(socket);
//     socket.onerror = (err) => reject(err);
//   })
// }
