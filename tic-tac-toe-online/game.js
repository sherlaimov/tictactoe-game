const ROWS_COUNT = 10;
const COLS_COUNT = 10;
const field = document.querySelector('.field');

function generateCols(row, colsCount, rowId) {
  for (let i = 0; i < colsCount; i++) {
    const id = rowId * 10 + i + 1;
    const col = document.createElement('div');
    col.id = `c-${id}`;
    col.dataset.id = id;
    col.className = 'cell';
    row.appendChild(col);
  }
}

function generateRows(rowsCount, colsCount) {
  for (let i = 0; i < rowsCount; i++) {
    const row = document.createElement('div');
    row.className = 'row';
    generateCols(row, colsCount, i);
    field.appendChild(row);
  }
}

generateRows(ROWS_COUNT, COLS_COUNT);

// ****************************

let gameState;
const userParams = getUrlParams();
const classMap = {
  x: 'ch',
  o: 'r'
};

function renderGame() {
  const allCells = [...document.querySelectorAll('.cell')];
  allCells.forEach(cell => {
    cell.removeAttribute('class');
    cell.classList.add('cell');
  });
  const { ended, reserved, step, win } = gameState;
  const messSpan = document.querySelector('#info-title > .message');
  if (step === userParams.side) {
    messSpan.textContent = 'Ваш ход';
  } else {
    messSpan.textContent = 'Ожидайте';
    movePolling();
  }
  Object.keys(reserved).forEach(key => {
    if (reserved[key].length) {
      reserved[key].forEach(cellNum => {
        const targetCell = allCells.find(cell => parseInt(cell.dataset.id, 10) === cellNum);
        targetCell.classList.add(classMap[key]);
      });
    }
  });
}

function getUrlParams() {
  const urlParams = new URLSearchParams(window.location.search);
  const userId = urlParams.get('userId');
  const gameId = urlParams.get('gameId');
  const side = urlParams.get('side');
  return { userId, gameId, side };
}

async function getGameStateAndRender() {
  const { gameId, userId } = userParams;
  const headers = new Headers({
    'Game-ID': gameId,
    'Player-ID': userId
  });
  const res = await fetch('http://xo.t.javascript.ninja/game', { headers });
  gameState = await res.json();
  console.log('gameState', gameState);
  renderGame();
}

getGameStateAndRender();

async function handleCellClick(e) {
  try {
    const cellId = e.target.dataset.id;
    console.log('JSON.stringify(cellId)', JSON.stringify(cellId));
    const { gameId, userId, side } = userParams;
    const headers = new Headers({
      'Content-Type': 'application/json',
      'Game-ID': gameId,
      'Player-ID': userId
    });
    const res = await fetch('http://xo.t.javascript.ninja/move', {
      method: 'POST',
      headers,
      body: JSON.stringify({ move: cellId })
    });
    const data = await res.json();
    if (res.status === 200) {
      // e.target.classList.add(classMap[side]);
      getGameStateAndRender();
    } else if (res.status === 410) {
      getGameStateAndRender();
    } else {
      const { message } = data;
      message ? errorMessage(message) : errorMessage('Неизвестная ошибка');
      // рекратить выполнение любой логики, связанной с игрой кроме кнопки новой игры
    }
    const { ended, win } = data;

    console.log('cellClick', data);
  } catch (err) {
    console.log('***Error', err);
    errorMessage(err.message);
  }
}

function errorMessage(title) {
  const block = document.querySelector('#alert');
  block.textContent = title;
  block.style.display = 'block';
}

function listenTo({ event, selector, handler }) {
  document.addEventListener(event, e => {
    if (e.target.matches(selector)) {
      handler(e);
      renderGame(gameState);
    }
  });
}

function bindEvents() {
  listenTo({
    event: 'click',
    selector: '.cell',
    handler: handleCellClick
  });
}

bindEvents();

function movePolling() {
  const { gameId, userId } = userParams;
  const headers = new Headers({
    'Game-ID': gameId,
    'Player-ID': userId
  });
  const xhr = fetch('http://xo.t.javascript.ninja/move', { headers })
    .then(r => {
      if (r.status === 200) {
        return r;
      }
      movePolling();
    })
    .then(r => r.json())
    .then(r => {
      console.log('movePolling', r);
      const { move, ended, win } = r;
      getGameStateAndRender();
      // setInterval(movePolling, 5000);
    })
    .catch(e => {
      console.log('Long-Polling Error', e);
      movePolling();
      // setInterval(movePolling, 5000);
    });
}
