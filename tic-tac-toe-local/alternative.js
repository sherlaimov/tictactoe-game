function TicTacToeGame(sideLength) {
  sideLength = sideLength || 3;

  var X_PIECE = "X";
  var O_PIECE = "O";

  var board = new Board(sideLength, sideLength);

  this.addX = function(pieceX, pieceY) {
      board.setSquare(pieceX, pieceY, X_PIECE);
  };

  this.addO = function(pieceX, pieceY) {
      board.setSquare(pieceX, pieceY, O_PIECE);
  };

  this.getBoard = function() {
      return board;
  }

  this.checkForWin = function() {
      return checkRows() || checkCols() || checkDiags();
  }

  this.toString = function() {
      return board.toString();
  }

  function checkRows() {
      for (var row = 0; row < sideLength; row++) {
          var initialPiece = board.getSquare(0, row);

          if (initialPiece === board.getEmptyPlaceholder()) {
              continue;
          }

          for (var col = 1; col < sideLength; col++) {
              var currentPiece = board.getSquare(col, row);

              if (currentPiece !== initialPiece) {
                  break;

              } else if (col === sideLength - 1) {
                  return initialPiece;
              }
          }

          if (row === sideLength - 1) {
              return false;
          }
      }

      return false;
  }

  function checkCols() {
      for (var col = 0; col < sideLength; col++) {
          var initialPiece = board.getSquare(col, 0);

          if (initialPiece === board.getEmptyPlaceholder()) {
              continue;
          }

          for (var row = 1; row < sideLength; row++) {
              var currentPiece = board.getSquare(col, row);

              if (currentPiece !== initialPiece) {
                  break;

              } else if (row === sideLength - 1) {
                  return initialPiece;
              }
          }

          if (col === sideLength - 1) {
              return false;
          }
      }

      return false;
  }

  //(2,0), (1,1), (0,2)

  function checkDiags() {
      //Check main diagonal
      //TODO Move topLeftPiece out of loop, and collapse if
      var topLeftPiece = board.getSquare(0, 0);
      for (var d = 0; d < sideLength; d++) {
          if (topLeftPiece === board.getEmptyPlaceholder()) {
              break;
          } 

          var currentPiece = board.getSquare(d, d);

          if (currentPiece !== topLeftPiece) {
              break;

          } else if (d === sideLength - 1) {
              return currentPiece;
          }

      }

      var topRightPiece = board.getSquare(sideLength - 1, 0);
      //Check inverse diagonal
      for (var d = 0; d < sideLength; d++) {
          if (topRightPiece === board.getEmptyPlaceholder()) {
              break;
          }             

          var currentPiece = board.getSquare(sideLength - d - 1, d);

          if (currentPiece !== topRightPiece) {
              break;

          } else if (d === sideLength - 1) {
              return currentPiece;
          }

      }

      return false;
  }
}

function Board(squaresWide, squaresHigh) {
  var self = this;

  var EMPTY_PLACEHOLDER = null;

  var dimensions = {
      width:  squaresWide,
      height: squaresHigh
  };

  var squares = createBoardArray(dimensions.width, dimensions.height);

  this.setSquare = function(x, y, object) {
      assertIsInBounds(x, y);

      squares[ getIndexOfSquare(x, y) ] = object;
  };

  this.getSquare = function(x, y) {
      assertIsInBounds(x, y);

      return squares[ getIndexOfSquare(x, y) ];
  };

  this.squareIsOccupied = function(x, y) {
      return this.isInBounds(x, y)
              && getSquare(x,y) !== EMPTY_PLACEHOLDER;
  }

  this.swapSquares = function(x1, y1, x2, y2) {
      assertIsInBounds(x1, y1);
      assertIsInBounds(x2, y2);

      var old1 = this.getSquare(x1, y1);
      var old2 = this.getSquare(x2, y2);

      this.setSquare(x1, y1, old2);
      this.setSquare(x2, y2, old1);
  }

  this.clearBoard = function() {
      squares = createBoardArray(dimensions.width, dimensions.height);
  };

  this.getEmptyPlaceholder = function() {
      return EMPTY_PLACEHOLDER;
  }

  /**
   * 
   * @param {Function} f function(x, y, cellContents)
   * @returns {undefined}
   */
  this.forEach = function(f) {
      for (var y = 0; y < dimensions.height; y++) {
          for (var x = 0; x < dimensions.width; x++) {
              f(x, y, this.getSquare(x, y));
          }
      }
  }


  this.toString = function() {
      var stringArr = [];

      for (var i = 0; i < squares.length; i++) {
          stringArr.push(
              squares[i] === EMPTY_PLACEHOLDER ?
                  '-' : squares[i]
          );

          if ((i + 1) % dimensions.width === 0) {
              stringArr.push('\n');

          } else {
              stringArr.push(' | ');
          }
      }

      return stringArr.join('');
  };

  this.isInBounds = function(x, y) {
      return  x >= 0 && x < dimensions.width &&
              y >= 0 && y < dimensions.height;
  }

  function assertIsInBounds(x, y) {
      if (!self.isInBounds(x, y)) {
          throw new Error("Coord out of bounds: (" + x + ", " + y + ")");
      }
  }

  function getIndexOfSquare(x, y) {
      return y * dimensions.width + x;
  }

  function createBoardArray(width, height) {
      var arr = [];

      var total = width * height;
      for (var i = 0; i < total; i++) {
          arr.push(EMPTY_PLACEHOLDER);
      }

      return arr;
  }

}