// Stuff Currently Not Used

function Point(x, y) {
  this.x = x;
  this.y = y;
  return this;
}

function getCoordinates(cId) {
  const row = Math.floor(cId / 3);
  const cell = cId % 3;
  return new Point(cell, row);
}

function getCellId(x, y) {
  return y * 3 + x;
}

function createBoard(x, y) {
  let result = [];
  for (let i = 0; i < x; i++) {
    result[i] = [];
    for (let j = 0; j < y; j++) {
      result[i][j] = -1;
    }
  }
  return result;
}

function checkForWin(board, toWin) {
  let prevX = false;
  let toWinHoriz, toWinVert, toWinDiagRight, toWinDiagLeft;
  toWinHoriz = toWinVert = toWinDiagRight = toWinDiagLeft = 0;

  for (var cell = 0; cell < board.length; cell++) {
    let prevY = false;
    for (var row = 0; row < board[0].length; row++) {
      //check for vertical pattern
      if (board[row][cell] >= 0 && (board[row][cell] === prevY || prevY === false)) {
        toWinVert++;
        if (toWinVert >= toWin) {
          return board[row][cell];
        }
      } else {
        toWinVert = 0;
      }
      prevY = board[row][cell];

      // check for horiz pattern
      if (board[cell][row] >= 0 && (board[cell][row] === prevX || prevX === false)) {
        toWinHoriz++;
        if (toWinHoriz >= toWin) {
          return board[cell][row];
        }
      } else {
        toWinHoriz = 0;
      }
    }
    if (board[cell][row]) {
      prevX = board[cell][row];
    } else {
      prevX = false;
    }
  }

  return false;
}

const board = createBoard(5, 5);
// board.map(row => (row[4] = 0));

// top-left to bottom-right
// board.map((row, i) => (row[i] = 0));
// console.log(board.every((row, i) => row[i] === 0));

// top-right to bottom-left
// board.map((row, i) => (row[row.length - 1 - i] = 0));
// console.log(board.every((row, i) => row[row.length - 1 - i] === 0));

// horizontal
// board.map((row, i) => (i === 1 ? row.map(el => 0) : row));
// board[2] = [0, 0, 0, 0, 0];
// console.log(board.some(row => row.every(el => el === 0)));

// vertical
// board.map(row => (row[2] = 0));
// console.log(board.some((row, i) => row[i] === 0));

const win = checkForWin(board, 5);
console.log(win);
