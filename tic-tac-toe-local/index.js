const appState = {
  moves: [],
  cache: [],
  gameWon: false,
};

const undoBtn = document.querySelector('.undo-btn');
const redoBtn = document.querySelector('.redo-btn');

function setStorage(name, data) {
  localStorage.setItem(`${name}`, JSON.stringify(data));
}

function renderGame(appState) {
  [...document.querySelectorAll('.cell')].forEach(cell => {
    cell.removeAttribute('class');
    cell.classList.add('cell');
  });
  document.querySelector('.won-title').classList.add('hidden');
  const crosses = new Set();
  const zeros = new Set();
  const wonMessage = document.querySelector('.won-message');

  appState.moves.forEach((move, i) => {
    if (i % 2 === 0) {
      // Crosses
      document.querySelector(`#c-${move}`).classList.add('ch');
      crosses.add(move);
    } else {
      document.querySelector(`#c-${move}`).classList.add('r');
      zeros.add(move);
    }
  });

  if (whoWins(crosses)) {
    renderWinner(crosses);
    wonMessage.textContent = 'Crosses won!';
  }
  if (whoWins(zeros)) {
    renderWinner(zeros);
    wonMessage.textContent = 'Toes won!';
  }
  if (appState.moves.length === 9) {
    document.querySelector('.won-title').classList.remove('hidden');
    wonMessage.textContent = "It's a draw!";
  }
  setUndoRedo();
}

function setUndoRedo() {
  if (appState.moves.length === 0) {
    undoBtn.disabled = true;
  } else {
    undoBtn.disabled = false;
  }
  if (appState.cache.length > 0) {
    redoBtn.disabled = false;
  } else {
    redoBtn.disabled = true;
  }
  if (appState.gameWon || appState.moves.length === 9) {
    undoBtn.disabled = true;
    redoBtn.disabled = true;

  }
}

const winConditions = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [6, 4, 2],
];

const getWonNums = moves => winConditions.find(row => row.every(val => moves.has(val)));

const whoWins = moves => winConditions.some(row => row.every(val => moves.has(val)));

const renderWinner = moves => {
  appState.gameWon = true;
  const wonNums = getWonNums(moves);
  wonNums.forEach(num => {
    document.querySelector(`#c-${num}`).classList.add('win');
    document.querySelector(`#c-${num}`).classList.add(getDirectionClass(wonNums));
  });
  document.querySelector('.won-title').classList.remove('hidden');

};

const getDirectionClass = nums => {
  const [first, second] = nums;
  if (Math.abs(second - first) === 4) {
    return 'diagonal-right';
  }
  if (Math.abs(second - first) === 2) {
    return 'diagonal-left';
  }
  if (Math.abs(second - first) === 3) {
    return 'vertical';
  }
  if (Math.abs(second - first) === 1) {
    return 'horizontal';
  }
};

function listenTo({ event, selector, handler }) {
  document.addEventListener(event, e => {
    if (e.target.matches(selector)) {
      handler(e);
      renderGame(appState);
    }
  });
}

function handleCellClick(e) {
  if (appState.gameWon) return;
  if (appState.cache.length > 0) {
    appState.cache.length = 0;
    setStorage('cache', appState.cache);
  }
  const cellId = parseInt(e.target.dataset.id);
  if (appState.moves.includes(cellId)) {
    return;
  }
  appState.moves.push(cellId);
  setStorage('moves', appState.moves);
}

function handleUndo(e) {
  if (appState.moves.length === 0) return;
  const undo = appState.moves.pop();
  setStorage('moves', appState.moves);
  appState.cache.push(undo);
  setStorage('cache', appState.cache);
}
function handleRedo(e) {
  if (appState.cache.length === 0) return;
  const redo = appState.cache.pop();
  setStorage('cache', appState.cache);
  appState.moves.push(redo);
  setStorage('moves', appState.moves);
}
function handleRestart(e) {
  resetAppState();
}

function resetAppState() {
  appState.moves.length = 0;
  appState.cache.length = 0;
  localStorage.removeItem('moves');
  localStorage.removeItem('cache');
  appState.gameWon = false;
}

function bindEvents() {
  listenTo({
    event: 'click',
    selector: '.cell',
    handler: handleCellClick,
  });
  listenTo({
    event: 'click',
    selector: '.undo-btn',
    handler: handleUndo,
  });
  listenTo({
    event: 'click',
    selector: '.redo-btn',
    handler: handleRedo,
  });
  listenTo({
    event: 'click',
    selector: '.restart-btn',
    handler: handleRestart,
  });
}
bindEvents();

// POPULATE GAME CELLS
if (!!localStorage.getItem('moves')) {
  populateMoves();
}
if (!!localStorage.getItem('cache')) {
  populateCache();
}
function populateMoves() {
  const moves = JSON.parse(localStorage.getItem('moves'));
  appState.moves = moves;
  renderGame(appState);
}
function populateCache() {
  const cache = JSON.parse(localStorage.getItem('cache'));
  appState.cache = cache;
  renderGame(appState);
}

window.addEventListener('storage', e => {
  console.log(e);
  if (e.key === 'moves' && e.newValue !== null) {
    appState.moves = JSON.parse(e.newValue);
  }
  if (e.key === 'cache' && e.newValue !== null) {
    appState.cache = JSON.parse(e.newValue);
  }
  if (e.key === 'moves' && e.newValue === null) {
    // game has been restarted
    resetAppState();
  }
  renderGame(appState);
});

window.addEventListener('beforeunload', () => {
  if (appState.gameWon) {
    resetAppState();
  }
});
